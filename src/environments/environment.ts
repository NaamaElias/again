// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDQLzyoAmU04NeTrOymW-ucPEC3itrbNBk",
    authDomain: "again-24d2f.firebaseapp.com",
    projectId: "again-24d2f",
    storageBucket: "again-24d2f.appspot.com",
    messagingSenderId: "954591124890",
    appId: "1:954591124890:web:3d315812ad35941d8b6e23"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
