import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lmbdafunction',
  templateUrl: './lmbda-function.component.html',
  styleUrls: ['./lmbda-function.component.css']
})
export class LmbdaFunctionComponent implements OnInit {

  result:string;

  constructor(private lmbdaService:LambdaService) { }

  sendData(education:number,income:number){
    this.lmbdaService.predict(education,income).subscribe(
      res => {console.log(res);
        this.result = res;
      })
    }

  ngOnInit(): void {
  }

}
