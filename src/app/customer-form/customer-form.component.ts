import { Customer } from './../interfaces/customer';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  name:string;
  education:number;
  income:number;
  @Output() update = new EventEmitter<Customer>();
  @Output() closeForm = new EventEmitter<null>();
  isError:boolean = false;

  updateParent(){
    if (this.education<0 || this.education>24){
      this.isError = true;
      this.education = null;
    }else{
    let customer:Customer = {name:this.name, education:this.education, income:this.income};
      this.update.emit(customer);
      this.name = null;
      this.education = null;
      this.income = null; 
      alert("Customer added to DB");
    }  
    
  }

  tellParentToClose(){
    this.closeForm.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
