import { LambdaService } from './../lambda.service';
import { CustomersService } from './../customers.service';
import { Customer } from './../interfaces/customer';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId:string;
  customers$;
  customers:Customer[];
  hiddenForm = false;
  rowToEdit:number = -1;
  customerToEdit:Customer = {name:null, education:null, income:null}
  isError = [];
  result:string;
  saved:boolean;

  displayedColumns: string[] = ['name', 'education', 'income','edit','predict','result','delete'];

  constructor(private customerService:CustomersService, public authService:AuthService, private lmbdaService:LambdaService) { }

  add(customer:Customer){
    this.customerService.addCustomer(this.userId, customer.name, customer.education, customer.income);
  }

  delete(customerId:string){
    this.customerService.deleteCustomer(this.userId,customerId);
  }
  
  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.education = this.customers[index].education;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }

  update(){
    if(this.customerToEdit.education<0 || this.customerToEdit.education>24){
      this.isError[this.rowToEdit] = true;
    }else{
      this.isError[this.rowToEdit] = false;
      let id = this.customers[this.rowToEdit].id;
      this.customerService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
      this.rowToEdit = null;
    }
  }

  predict(i,education:number, income:number){
    this.lmbdaService.predict(education,income ).subscribe(
      res => {console.log(res);
        this.result = res;
        if(res>=0.5){
          this.result = "Will pay";
        }else{
          this.result = "will not pay";
        }
      this.customers[i].result = this.result;
    }
    );     
  }

  updateResult(i,result){
    this.customerService.updatePredict(this.userId,this.customers[i].id,result);
    this.customers[i].saved = true;
  }
  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.customers$ = this.customerService.getCustomers(this.userId);
        this.customers$.subscribe(
          docs => {
            this.customers = [];
            for (let document of docs){
              const customer:Customer = document.payload.doc.data();
              if(customer.result){
                customer.saved = true;
              }
              customer.id = document.payload.doc.id;
              this.customers.push(customer);
            }
          }
        )
      }
    )
  }


}
