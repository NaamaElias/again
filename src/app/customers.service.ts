import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`);
    return this.customerCollection.snapshotChanges();
  }

  addCustomer(userId:string,name:string,education:number, income:number){
     const customer = {name:name, education:education, income:income};
     this.userCollection.doc(userId).collection('customers').add(customer);
  }

  deleteCustomer(userId:string,customerId:string){
    this.db.doc(`users/${userId}/customers/${customerId}`).delete();
  }

  updateCustomer(userId:string, customerId, name:string,education:number, income:number){
    this.db.doc(`users/${userId}/customers/${customerId}`).update(
      {
        name:name,
        education:education,
        income:income
      }
    )
  }

  updatePredict(userId:string, customerId, result){
    this.db.doc(`users/${userId}/customers/${customerId}`).update(
      {
        result:result
      }
    )
  }

  constructor(private db:AngularFirestore) { }
}
